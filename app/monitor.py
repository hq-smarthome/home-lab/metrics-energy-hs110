import socket
import json
import os
import time
import threading
from struct import pack
from datetime import datetime
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

for var in ["DEVICE_IP", "DEVICE_ALIAS", "INFLUXDB_HOST", "INFLUXDB_ORG", "INFLUXDB_BUCKET", "INFLUXDB_TOKEN"]:
  if var not in os.environ:
    raise EnvironmentError("Failed because {} is not set.".format(var))

influxClient = InfluxDBClient(url=os.environ['INFLUXDB_HOST'], token=os.environ['INFLUXDB_TOKEN'], org=os.environ['INFLUXDB_ORG'])
influxWriteApi = influxClient.write_api(write_options=SYNCHRONOUS)

# Encryption of TP-Link Smart Home Protocol
# XOR Autokey Cipher with starting key = 171
def encrypt(string):
  key = 171
  result = pack('>I', len(string))

  for i in string:
    a = key ^ ord(i)
    key = a
    result += bytes([a])

  return result

# Decryption of TP-Link Smart Home Protocol
# XOR Autokey Cipher with starting key = 171
def decrypt(string):
  key = 171
  result = ""

  for i in string:
    a = key ^ i
    key = i
    result += chr(a)

  return result

# Sends a command to the TP-Link Smart device
def sendCommand(cmd, ip):
  sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  sock_tcp.settimeout(20)
  sock_tcp.connect((ip, 9999))
  sock_tcp.settimeout(None)
  sock_tcp.send(encrypt(cmd))
  data = sock_tcp.recv(2048)
  sock_tcp.close()

  return json.loads(decrypt(data[4:]))

def getMeasurementPoint(ip):
  try:
    timestamp = int(round(time.time() * 1000000000))
    dInfo = sendCommand('{"system":{"get_sysinfo":{}}}', ip)['system']['get_sysinfo']
    dData = sendCommand('{"emeter":{"get_realtime":{}}}', ip)['emeter']['get_realtime']

    print("Voltage: " + str(dData['voltage_mv']) + "mV, Current: " + str(dData['current_ma']) + "mA, Power: " + str(dData['power_mw']) + "mW, Energy: " + str(dData['total_wh']) + "Wh")

    return Point("smart_plug") \
      .tag("model", dInfo['model']) \
      .tag("mac", dInfo['mac']) \
      .tag("rssi", dInfo['rssi']) \
      .tag("alias", os.environ['DEVICE_ALIAS']) \
      .tag("deviceId", dInfo['deviceId']) \
      .tag("source", "TP-Link HS110") \
      .tag("country", "SE") \
      .field("voltage", dData['voltage_mv']) \
      .field("current", dData['current_ma']) \
      .field("power", dData['power_mw']) \
      .field("energy", dData['total_wh']) \
      .time(timestamp)

  except socket.error:
    print("Could not connect to plug " + ip )

def measureEnergy():
  point = getMeasurementPoint(os.environ['DEVICE_IP'])
  influxWriteApi.write(os.environ['INFLUXDB_BUCKET'], os.environ['INFLUXDB_ORG'], point)

# Simple loop for now
while True:
  measureEnergy()
  time.sleep(15)
